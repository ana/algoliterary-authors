#!/usr/bin/python -tt
# -*- coding: utf-8 -*-

from nltk.tokenize import word_tokenize
import re
import string
import codecs
import glob

outputfilename = 'data.txt'
input_patt = 'input/*.txt'

regex = re.compile('[%s]' % re.escape(string.punctuation)) #see documentation here: http://docs.python.org/2/library/string.html
toremove = ['”', '“', '«', '»']

with codecs.open(outputfilename, "w", "utf-8") as destination:
	for path in glob.glob(input_patt):
		print('** READING: ', path)
		sentences = []

		with codecs.open(path, 'r', "utf-8") as source:
			lines = source.readlines()
			if len(lines) == 1:
				lines = lines.split('. ')
			print (lines)
			for line in lines:
				for chr in toremove:
					line = line.replace(chr, '')

				words = word_tokenize(line)
				print (words)
				string = []
				for word in words:
					word = word.strip()

					if word:
						string.append(word)

				sentences.append(' '.join(string))

		for sentence in sentences:
			destination.write(sentence.strip().lower()+" ")
		print ('*text is stripped*')
