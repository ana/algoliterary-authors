#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 28 17:00:51 2021

"""
#NECESITAS INSTALAR:
# pip install -U spacy
# python -m spacy download en_core_web_sm

#IMPORTAMOS MÓDULOS________________________________________________________

import spacy
import random
# Cargar tokenizer en español
nlp = spacy.load("es_core_news_sm")

#FUNCIONES_________________________________________________________________

def limpiar_texto(fragmento):
    fragmento_limpio = fragmento.split('--')
    fragmento_limpio = ', '.join(fragmento_limpio)
    fragmento_limpio = fragmento.split('\n')
    fragmento_limpio = ' '.join(fragmento_limpio)
    return fragmento_limpio

def crear_base_datos(nombre_texto):
    # Abrir el archivo de texto para crear la base de datos
    archivo = open(nombre_texto, 'r')
    fragmento = archivo.read()
    archivo.close()
    fragmento_limpio = limpiar_texto(fragmento)
    
    # Tokenización del fragmento de texto
    doc = nlp(fragmento_limpio)
    doc_len = len(doc)

    palabras_arboles = {} #Verbos, sustantivos, adverbios y adjetivos
    palabras_camino = {} #El resto de palabras
    for i in range(0, doc_len-1):
        tok = doc[i]
        palabra = tok.text.lower()
        sig_palabra = doc[i+1].text.lower()
        if (tok.pos_ == 'VERB' or tok.pos_ == 'NOUN'\
            or tok.pos_ == 'ADJ' or tok.pos_ == 'ADV'):
            if palabra in palabras_arboles:
                palabras_arboles[palabra].append(sig_palabra)
            else:
                palabras_arboles[palabra] = [sig_palabra]
        else:
            if palabra in palabras_camino:
                palabras_camino[palabra].append(sig_palabra)
            else:
                palabras_camino[palabra] = [sig_palabra]
    return (palabras_arboles, palabras_camino)

#Genera la siguiente palabra en el camino según la cadena de Markov
def paso(palabra, palabras_arboles, palabras_camino):
    if palabra in palabras_arboles:
        return random.choice(palabras_arboles[palabra])
    elif palabra in palabras_camino :
        return random.choice(palabras_camino[palabra])
    else:
        pass

def camino(palabra, palabras_arboles, palabras_camino):
    i = 0
    itinerario = palabra.capitalize() + ' '
    while palabra != '.' and i < 100:
        delimitador = ' '
        palabra = paso(palabra, palabras_arboles, palabras_camino)
        if palabra in '.,:;!?\)':
            itinerario = itinerario[:-1]
        aliento = random.random()
        if aliento < 0.1:
            delimitador = '\n'
        itinerario += (palabra + delimitador)
        i += 1
    return itinerario

#Genera un camino a partir de un texto y una palabra del texto
def crear_camino(nombre_archivo, palabra_inicial):
    (palabras_arboles, palabras_camino) = crear_base_datos(nombre_archivo)
    return camino(palabra_inicial, palabras_arboles, palabras_camino)

#EJECUCIÓN__________________________________________________________________
print(crear_camino('emilia_prueba.txt', 'un'))